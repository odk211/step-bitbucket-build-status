# step-bitbucket-build-status

[![wercker status](https://app.wercker.com/status/e128fa14ff3ee1d61c407dbeb386f193/m "wercker status")](https://app.wercker.com/project/bykey/e128fa14ff3ee1d61c407dbeb386f193)

Wercker step to post the build status manually to Bitbucket via [Bitbucket API](https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/commit/%7Bnode%7D/statuses/build).

# Options:

- `username` (required) Bitbucket account user name.
- `password` (required) App passwords of the account.
- `build-state` (optional) Build state to send. You do not need to set this option usually. Default: calculated value from `${WERCKER_RESULT}`.
- `build-key` (optional) Identifier for the status. Default: `WERCKER-STEP-BITBUCKET-BUILD-STATUS`.
- `build-name` (optional) Default: `Wercker CI`.
- `build-description` (optional) Default: empty string.
- `build-url` (optional) Default: `${WERCKER_BUILD_URL}`.
- `successful-build-name` (optional) Overwrite `build-name` when `SUCCESSFUL` state. (e.g. `Wercker CI passed`)
- `successful-build-description` (optional) Overwrite `build-description` when `SUCCESSFUL` state.
- `successful-build-url` (optional) Overwrite `build-url` when `SUCCESSFUL` state.
- `failed-build-name` (optional) Overwrite `build-name` when `FAILED` state. (e.g. `Wercker CI failed`)
- `failed-build-description` (optional) Overwrite `build-description` when `FAILED` state.
- `failed-build-url` (optional) Overwrite `build-url` when `FAILED` state.

# Example

```
build:
  steps:
    - odk211/bitbucket-build-status: # Post `INPROGRESS` status.
        username: ${BITBUCKET_USER_NAME}
        password: ${BITBUCKET_USER_PASSWORD}
    - script:
        name: test
        code: ./test.sh
  after-steps:
    - odk211/bitbucket-build-status: # Post `SUCCESSFUL` or `FAILED` status.
        username: ${BITBUCKET_USER_NAME}
        password: ${BITBUCKET_USER_PASSWORD}
        successful-build-name: Wercker CI passed
        failed-build-name: Wercker CI failed
        failed-build-description: "Step '${WERCKER_FAILED_STEP_DISPLAY_NAME}' failed. ${WERCKER_FAILED_STEP_MESSAGE}"
```

```
deploy:
  steps:
    - odk211/bitbucket-build-status:
        username: ${BITBUCKET_USER_NAME}
        password: ${BITBUCKET_USER_PASSWORD}
        build-key: HEROKU-REVIEW
        build-name: Heroku review
    - odk211/heroku-platform-api-deploy:
        key: ${HEROKU_KEY}
        app-name: ${HEROKU_APP_NAME}
  after-steps:
    - odk211/bitbucket-build-status:
        username: ${BITBUCKET_USER_NAME}
        password: ${BITBUCKET_USER_PASSWORD}
        build-key: HEROKU-REVIEW
        build-name: Heroku review
        successful-build-url: ${HEROKU_APP_URL}
```